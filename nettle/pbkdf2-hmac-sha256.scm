(define-module (nettle pbkdf2-hmac-sha256))

(use-modules (nettle internal)
             (rnrs bytevectors))

(re-export pbkdf2-hmac-sha256!)

(export pbkdf2-hmac-sha256)

(define (pbkdf2-hmac-sha256 password salt length iterations)
  (let ((password (string->utf8 password))
        (salt (string->utf8 salt))
        (out-bv (make-bytevector length)))
    (pbkdf2-hmac-sha256! iterations password 0 (bytevector-length password)
                         salt 0 (bytevector-length salt)
                         out-bv 0 (bytevector-length out-bv))
    out-bv))
