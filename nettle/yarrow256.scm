(define-module (nettle yarrow256)
  #:use-module (nettle internal))

(re-export make-yarrow256-ctx
           yarrow256-random!)
