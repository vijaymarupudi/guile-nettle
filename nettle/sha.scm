(define-module (nettle sha))

(use-modules (nettle internal))

(re-export bytevector->sha1
           bytevector->sha256)

