(define-module (nettle hmac-sha256))

(use-modules (nettle internal))

(re-export bytevector->hmac-sha256)
