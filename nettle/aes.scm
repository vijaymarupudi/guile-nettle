(define-module (nettle aes)
  #:use-module (nettle internal)
  #:use-module (rnrs bytevectors))

(re-export make-aes256-ctx
           aes256-set-encrypt-key!
           aes256-set-decrypt-key!
           aes256-encrypt!
           aes256-decrypt!

           make-aes256-cbc-ctx
           aes256-cbc-set-encrypt-key!
           aes256-cbc-set-decrypt-key!
           aes256-cbc-encrypt!
           aes256-cbc-decrypt!)

(export aes256-cbc-pbkdf2-hmac-sha255-encrypt-bytevector
        aes256-cbc-pbkdf2-hmac-sha255-decrypt-bytevector
        aes256-cbc-encrypt-bytevector
        aes256-cbc-decrypt-bytevector)

(define (string->key password salt)
  (let ((password (if (bytevector? password)
                   password
                   (string->utf8 password)))
        (salt (if (bytevector? salt)
                  salt
                  (string->utf8 salt)))
        (key (make-bytevector 32)))
    (pbkdf2-hmac-sha256! 100000
                         password 0 (bytevector-length password)
                         salt 0 (bytevector-length salt)
                         key 0 (bytevector-length key))
    key))

(define IV #vu8(188 121 80 133 180 78 130 178 212 246 28 21 89 167 78 17))


(define (aes256-cbc-encrypt-bytevector key bv)
  (let* ((bv-len (bytevector-length bv))
         (ctx (make-aes256-cbc-ctx IV)))
    
    (aes256-cbc-set-encrypt-key! ctx key)
    
    (let* ((r (modulo bv-len 16))
           (temp (make-bytevector 16))
           (out-bv (make-bytevector (+ (- bv-len r) 16) 0))
           (boundary (- bv-len r)))

      
      ;; Encrypt the perfect blocks
      (aes256-cbc-encrypt! ctx bv 0 boundary
                           out-bv 0
                           boundary)


      ;; Copy the extra stuff
      (bytevector-copy! bv boundary
                        temp 0
                        (- bv-len boundary))

      
      
      ;; Add 1 bit
      (bytevector-u8-set! temp r #x80)

      (aes256-cbc-encrypt! ctx temp 0 16
                           out-bv boundary (+ boundary 16))



      out-bv
      
      )))


(define (aes256-cbc-pbkdf2-hmac-sha255-encrypt-bytevector password salt bv)
  (aes256-cbc-encrypt-bytevector (string->key password salt) bv))

(define (aes256-cbc-decrypt-bytevector key enc-bv)
  (let* ((decrypted-bv (make-bytevector (bytevector-length enc-bv)))
         (ctx (make-aes256-cbc-ctx IV)))
    
    (aes256-cbc-set-decrypt-key! ctx key)

    (aes256-cbc-decrypt! ctx enc-bv 0 (bytevector-length enc-bv)
                         decrypted-bv 0 (bytevector-length enc-bv))

    (let* ((stop-index (do ((i (- (bytevector-length enc-bv) 1) (- i 1)))
                           ((= (bytevector-u8-ref decrypted-bv i) #x80) i)))
           (out-bv (make-bytevector stop-index)))
      (bytevector-copy! decrypted-bv 0 out-bv 0 stop-index)
      out-bv)))


(define (aes256-cbc-pbkdf2-hmac-sha255-decrypt-bytevector password salt bv)
  (aes256-cbc-decrypt-bytevector (string->key password salt) bv))

