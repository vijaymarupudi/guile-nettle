(define-module (nettle internal))

(export
 bytevector->sha1
 bytevector->sha256
 bytevector->hmac-sha256

 string->base64
 base64->bytevector
 make-base64-encode-ctx
 base64-encode!
 base64-encode-length
 base64-encode-final!
 BASE_64_ENCODE_FINAL_LENGTH

 pbkdf2-hmac-sha256!

 make-yarrow256-ctx
 yarrow256-random!

 
 make-aes256-ctx
 aes256-set-encrypt-key!
 aes256-set-decrypt-key!
 aes256-encrypt!
 aes256-decrypt!

 make-aes256-cbc-ctx
 aes256-cbc-set-encrypt-key!
 aes256-cbc-set-decrypt-key!
 aes256-cbc-encrypt!
 aes256-cbc-decrypt!)

(load-extension "libguile-nettle" "scm_nettle_init")
