(define-module (nettle base64))

(use-modules (nettle internal)
             (rnrs bytevectors))

(export
 bytevector->base64
 bytevector->base64-string
 string->base64-string
 base64-string->bytevector
 full-base64-encode-length

 base64->string
 base64-string->string)

(re-export string->base64
           base64->bytevector
           make-base64-encode-ctx
           base64-encode!
           base64-encode-length
           base64-encode-final!
           BASE_64_ENCODE_FINAL_LENGTH)



(define (base64-string->bytevector x)
  (base64->bytevector (string->utf8 x)))


(define (base64->string x)
  (utf8->string (base64->bytevector x)))


(define (base64-string->string x)
  (utf8->string (base64-string->bytevector x)))


(define (bytevector->base64 in-bv)
  (let* ((ctx (make-base64-encode-ctx))
         (out-bv-len (full-base64-encode-length (bytevector-length in-bv)))
         (out-bv (make-bytevector out-bv-len))
         (bytes-written (base64-encode! ctx out-bv 0 8128 in-bv 0 (bytevector-length in-bv))))
    (base64-encode-final! ctx out-bv bytes-written (bytevector-length out-bv))
    out-bv))

(define (bytevector->base64-string in-bv)
  (utf8->string (bytevector->base64 in-bv)))

(define (string->base64-string str)
  (utf8->string (string->base64 str)))

(define (full-base64-encode-length bv-len)
  (* 4 (ceiling-quotient bv-len 3)))

