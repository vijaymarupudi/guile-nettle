#include <libguile.h>
#include <nettle/yarrow.h>
#include "scm_nettle.h"

SCM_DEFINE(scm_nettle_make_yarrow256, "make-yarrow256-ctx", 3, 0, 0, (SCM scm_seed_bv,
                                                                  SCM scm_seed_start,
                                                                  SCM scm_seed_end),
           "Make yarrow256_ctx") {
  char* seed_ptr;
  size_t seed_len;

  validate_bv_arg(1, scm_seed_bv, scm_seed_start, scm_seed_end, YARROW256_SEED_FILE_SIZE, &seed_ptr, &seed_len);

  SCM yarrow_ctx_bv = scm_c_make_bytevector(sizeof(struct yarrow256_ctx));

  struct yarrow256_ctx *ctx = (struct yarrow256_ctx *)SCM_BYTEVECTOR_CONTENTS(yarrow_ctx_bv);
  yarrow256_init(ctx, 0, NULL);
  yarrow256_seed(ctx, seed_len, seed_ptr);
  return yarrow_ctx_bv;
}

SCM_DEFINE(scm_nettle_yarrow256_random, "yarrow256-random!", 4, 0, 0, (SCM yarrow_ctx_bv,
                                                                      SCM scm_dst_bv,
                                                                      SCM scm_dst_start,
                                                                      SCM scm_dst_end),
           "Generate random bytes") {
  
  char* dst_ptr;
  size_t dst_len;

  validate_bv_arg(2, scm_dst_bv, scm_dst_start, scm_dst_end, 0, &dst_ptr, &dst_len);

  struct yarrow256_ctx *ctx = (struct yarrow256_ctx *)SCM_BYTEVECTOR_CONTENTS(yarrow_ctx_bv);

  yarrow256_random(ctx, dst_len, dst_ptr);
  return SCM_UNSPECIFIED;
  
}

void scm_nettle_yarrow256_init() {
#include "scm_nettle_yarrow256.x"
}
