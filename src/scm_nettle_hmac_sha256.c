#include <nettle/hmac.h>
#include <libguile.h>
#include "scm_nettle_hmac_sha256.h"
#include "scm_nettle.h"


SCM_DEFINE(scm_nettle_bytevector_to_hmac_sha256, "bytevector->hmac-sha256", 4, 0, 0, (SCM bv, SCM scm_start, SCM scm_end, SCM key_bv), "Hash bytevector using HMAC-SHA256")
{
  size_t len;
  char* buf;
  validate_bv_arg(1, bv, scm_start, scm_end, 0, &buf, &len);
  char* key_buf;
  size_t key_len;
  validate_bv_arg(4, key_bv, SCM_UNDEFINED, SCM_UNDEFINED, 0, &key_buf, &key_len);
  SCM ret_bv = scm_c_make_bytevector(SHA256_DIGEST_SIZE);
  char* dst_ptr = SCM_BYTEVECTOR_CONTENTS(ret_bv);
  struct hmac_sha256_ctx ctx;
  hmac_sha256_set_key(&ctx, key_len, key_buf);
  hmac_sha256_update(&ctx, len, buf);
  hmac_sha256_digest(&ctx, SHA256_DIGEST_SIZE, dst_ptr);
  return ret_bv;  
}

void scm_nettle_hmac_sha256_init() {
  #include "scm_nettle_hmac_sha256.x"
}
