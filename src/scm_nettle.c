#include "scm_nettle.h"
#include "scm_nettle_base64.h"
#include "scm_nettle_sha.h"
#include "scm_nettle_hmac_sha256.h"
#include "scm_nettle_pbkdf2_hmac_sha256.h"
#include "scm_nettle_yarrow256.h"
#include "scm_nettle_aes.h"
#include "scm_nettle_aes_cbc.h"

SCM scm_nettle_invalid_bounds;

void scm_nettle_init() {
  scm_nettle_invalid_bounds = scm_from_utf8_symbol("nettle-invalid-bounds");
  scm_nettle_base64_init();
  scm_nettle_sha_init();
  scm_nettle_hmac_sha256_init();
  scm_nettle_pbkdf2_hmac_sha256_init();
  scm_nettle_yarrow256_init();
  scm_nettle_aes_init();
  scm_nettle_aes_cbc_init();
}
