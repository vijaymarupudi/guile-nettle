#include <libguile.h>
#include <nettle/base64.h>
#include <stdbool.h>
#include "scm_nettle.h"

SCM scm_nettle_invalid_base64;

SCM_DEFINE(scm_nettle_make_base64_encode_ctx, "make-base64-encode-ctx", 0, 1, 0, (SCM scm_url_encode_p), "Make a base64 context")
{
  bool url_encode_p = SCM_UNBNDP(scm_url_encode_p) ? false : scm_to_bool(scm_url_encode_p);
  SCM bv = scm_c_make_bytevector(sizeof(struct base64_encode_ctx));
  if (url_encode_p) {
    base64url_encode_init((struct base64_encode_ctx*)SCM_BYTEVECTOR_CONTENTS(bv));
  } else {
    base64_encode_init((struct base64_encode_ctx*)SCM_BYTEVECTOR_CONTENTS(bv));
  }
  return bv;
}

SCM_DEFINE(scm_nettle_base64_encode_update, "base64-encode!", 7, 0, 0,
           (SCM scm_ctx, SCM scm_dst_bv, SCM scm_dst_start,
            SCM scm_dst_end, SCM scm_src_bv, SCM scm_src_start,
            SCM scm_src_end),
           "Encode into base64")
#define FUNC_NAME s_scm_nettle_base64_encode_update
{
  struct base64_encode_ctx *ctx;
  char *src, *dst;
  size_t src_len, dst_len, bytes_generated;

  ctx = (struct base64_encode_ctx*)SCM_BYTEVECTOR_CONTENTS(scm_ctx);

  validate_bv_arg(5, scm_src_bv, scm_src_start, scm_src_end, 0, &src, &src_len);
  validate_bv_arg(2, scm_dst_bv, scm_dst_start, scm_dst_end, src_len, &dst, &dst_len);
  
  if (dst_len < BASE64_ENCODE_LENGTH(src_len)) {
    scm_error(scm_nettle_invalid_bounds, FUNC_NAME, "invalid bounds", SCM_EOL, SCM_EOL);
  }

  bytes_generated = base64_encode_update(ctx, dst, src_len, src);
  return scm_from_size_t(bytes_generated);
}
#undef FUNC_NAME

SCM_DEFINE(scm_nettle_base64_encode_final, "base64-encode-final!", 4, 0, 0,
           (SCM scm_ctx, SCM scm_dst_bv, SCM scm_dst_start, SCM scm_dst_end),
           "Finalize encoding into base64")
#define FUNC_NAME s_scm_nettle_base64_encode_final
{
  struct base64_encode_ctx *ctx;
  char *dst;
  size_t dst_len, bytes_generated;

  ctx = (struct base64_encode_ctx*)SCM_BYTEVECTOR_CONTENTS(scm_ctx);
  validate_bv_arg(2, scm_dst_bv, scm_dst_start, scm_dst_end, 0, &dst, &dst_len);
  
  bytes_generated = base64_encode_final(ctx, dst);
  return scm_from_size_t(bytes_generated);
}
#undef FUNC_NAME

SCM_DEFINE(scm_nettle_string_to_base64, "string->base64", 1, 0, 0,
           (SCM str),
           "Encoding string into base64")
#define FUNC_NAME s_scm_nettle_string_to_base64
{
  char* buf;
  size_t len, bv_len, bytes_written;
  SCM bv;
  SCM_VALIDATE_STRING(1, str);
  buf = scm_to_utf8_stringn(str, &len);
  
  bv_len = 4 * ((len + 2) / 3) ;
  bv = scm_c_make_bytevector(bv_len);

  struct base64_encode_ctx ctx;

  base64_encode_init(&ctx);
  bytes_written = base64_encode_update(&ctx, SCM_BYTEVECTOR_CONTENTS(bv), len, buf);
  base64_encode_final(&ctx, SCM_BYTEVECTOR_CONTENTS(bv) + bytes_written);
  
  return bv;
}
#undef FUNC_NAME

static int padding_count(char* buf, size_t len) {

  if (len && buf[len - 1] == '=') {
    if ((len - 1) && buf[len - 2] == '=') {
      return 2;
    }
    return 1;
  }

  return 0;
}


SCM_DEFINE(scm_nettle_base64_to_bytevector, "base64->bytevector", 1, 0, 0,
           (SCM bv),
           "Decoding bytevector from base64")
#define FUNC_NAME s_scm_nettle_base64_to_bytevector
{
  char* buf;
  size_t len, dst_length;
  SCM out_bv;
  struct base64_decode_ctx ctx;
  int ret, padding;
  
  SCM_VALIDATE_BYTEVECTOR(1, bv);
  
  len = SCM_BYTEVECTOR_LENGTH(bv);
  buf = SCM_BYTEVECTOR_CONTENTS(bv);

  padding = padding_count(buf, len);
  
  out_bv = scm_c_make_bytevector(((len / 4) * 3) - padding);

  base64_decode_init(&ctx);

  ret = base64_decode_update(&ctx, &dst_length, SCM_BYTEVECTOR_CONTENTS(out_bv), len, buf);
  
  if (!ret) {
    scm_error(scm_nettle_invalid_base64, FUNC_NAME, "invalid base64", SCM_EOL, SCM_EOL);
  }

  ret = base64_decode_final(&ctx);
  
  if (!ret) {
    scm_error(scm_nettle_invalid_base64, FUNC_NAME, "invalid base64 padding", SCM_EOL, SCM_EOL);
  }

  return out_bv;
}
#undef FUNC_NAME

SCM_DEFINE(scm_nettle_base64_encode_length, "base64-encode-length", 1, 0, 0, (SCM scm_in_len),
           "Get the minimum length needed in a destination buffer to encode the output in.") {
  return scm_from_size_t(BASE64_ENCODE_LENGTH(scm_to_size_t(scm_in_len)));
}

void scm_nettle_base64_init() {
  scm_nettle_invalid_base64 = scm_from_utf8_symbol("nettle-invalid-base64");
  scm_c_define("BASE64_ENCODE_FINAL_LENGTH", scm_from_size_t(BASE64_ENCODE_FINAL_LENGTH));
#include "scm_nettle_base64.x"
}
