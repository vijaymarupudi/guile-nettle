#include <libguile.h>
#include <nettle/pbkdf2.h>
#include "scm_nettle.h"


SCM_DEFINE(scm_nettle_pbkdf2_hmac_sha256, "pbkdf2-hmac-sha256!", 10, 0, 0, (SCM scm_iterations,
                                                                          SCM scm_key_bv,
                                                                          SCM scm_key_start,
                                                                          SCM scm_key_end,
                                                                          SCM scm_salt_bv,
                                                                          SCM scm_salt_start,
                                                                          SCM scm_salt_end,
                                                                          SCM scm_dst_bv,
                                                                          SCM scm_dst_start,
                                                                          SCM scm_dst_end),
           "Use PBKDF2 to generate a key") {
  
  char* key_ptr;
  size_t key_len;
  char* salt_ptr;
  size_t salt_len;
  char* dst_ptr;
  size_t dst_len;
  unsigned int iterations;
  

  validate_bv_arg(2, scm_key_bv, scm_key_start, scm_key_end, 0, &key_ptr, &key_len);
  validate_bv_arg(5, scm_salt_bv, scm_salt_start, scm_salt_end, 0, &salt_ptr, &salt_len);
  validate_bv_arg(8, scm_dst_bv, scm_dst_start, scm_dst_end, 0, &dst_ptr, &dst_len);
  iterations = scm_to_uint(scm_iterations);
  pbkdf2_hmac_sha256(key_len, key_ptr, iterations, salt_len, salt_ptr, dst_len, dst_ptr);
  return SCM_UNSPECIFIED;
}

void scm_nettle_pbkdf2_hmac_sha256_init() {
#include "scm_nettle_pbkdf2_hmac_sha256.x"
}
