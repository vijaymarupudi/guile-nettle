#include <libguile.h>
#include <nettle/aes.h>
#include <nettle/cbc.h>
#include "scm_nettle.h"
#include <stdio.h>

static SCM aes256_cbc_error_key;

struct aes256_cbc_ctx CBC_CTX(struct aes256_ctx, AES_BLOCK_SIZE);

SCM_DEFINE(scm_nettle_make_aes256_cbc_ctx, "make-aes256-cbc-ctx", 1, 0, 0, (SCM iv), "Make AES 256 ctx") {
  SCM bv = scm_c_make_bytevector(sizeof(struct aes256_cbc_ctx));
  struct aes256_cbc_ctx* ctx = (struct aes256_cbc_ctx*) SCM_BYTEVECTOR_CONTENTS(bv);
  CBC_SET_IV(ctx, SCM_BYTEVECTOR_CONTENTS(iv));
  return bv;
}

SCM_DEFINE(scm_nettle_aes256_cbc_set_encrypt_key, "aes256-cbc-set-encrypt-key!", 2, 0, 0, (SCM ctx_bv, SCM keybv), "AES 256 set encrypt key") {
  if (scm_c_bytevector_length(keybv) < AES256_KEY_SIZE) {
    scm_error(aes256_cbc_error_key, "aes256-cbc-set-encrypt-key!", "Key too small", scm_list_1(keybv), SCM_EOL);
    return SCM_EOL;
  }
  aes256_set_encrypt_key(&(((struct aes256_cbc_ctx*)SCM_BYTEVECTOR_CONTENTS(ctx_bv))->ctx), SCM_BYTEVECTOR_CONTENTS(keybv));
  return SCM_BOOL_T;
}

SCM_DEFINE(scm_nettle_aes256_cbc_set_decrypt_key, "aes256-cbc-set-decrypt-key!", 2, 0, 0, (SCM ctx_bv, SCM keybv), "AES 256 set decrypt key") {
  if (scm_c_bytevector_length(keybv) < AES256_KEY_SIZE) {
    scm_error(aes256_cbc_error_key, "aes256-cbc-set-decrypt-key!", "Key too small", scm_list_1(keybv), SCM_EOL);
    return SCM_EOL;
  }
  aes256_set_decrypt_key(&(((struct aes256_cbc_ctx*)SCM_BYTEVECTOR_CONTENTS(ctx_bv))->ctx), SCM_BYTEVECTOR_CONTENTS(keybv));
  return SCM_BOOL_T;
}

SCM_DEFINE(scm_nettle_aes256_cbc_encrypt, "aes256-cbc-encrypt!", 7, 0, 0, (SCM ctxbv, SCM inbv, SCM inbvstart, SCM inbvend, SCM outbv, SCM outbvstart, SCM outbvend), "AES 256 encrypt") {
  char* in_ptr;
  size_t in_ptr_length;
  char* out_ptr;
  size_t out_ptr_length;

  validate_bv_arg(1, inbv, inbvstart, inbvend, 0, &in_ptr, &in_ptr_length);

  validate_bv_arg(1, outbv, outbvstart, outbvend, in_ptr_length, &out_ptr, &out_ptr_length);

  CBC_ENCRYPT((struct aes256_cbc_ctx*)SCM_BYTEVECTOR_CONTENTS(ctxbv), aes256_encrypt, in_ptr_length, out_ptr, in_ptr);

  return SCM_BOOL_T;
  
}

SCM_DEFINE(scm_nettle_aes256_cbc_decrypt, "aes256-cbc-decrypt!", 7, 0, 0, (SCM ctxbv, SCM inbv, SCM inbvstart, SCM inbvend, SCM outbv, SCM outbvstart, SCM outbvend), "AES 256 decrypt") {
  char* in_ptr;
  size_t in_ptr_length;
  char* out_ptr;
  size_t out_ptr_length;

  validate_bv_arg(1, inbv, inbvstart, inbvend, 0, &in_ptr, &in_ptr_length);

  validate_bv_arg(1, outbv, outbvstart, outbvend, in_ptr_length, &out_ptr, &out_ptr_length);

  CBC_DECRYPT((struct aes256_cbc_ctx*)SCM_BYTEVECTOR_CONTENTS(ctxbv), aes256_decrypt, in_ptr_length, out_ptr, in_ptr);

  return SCM_BOOL_T;
  
}

void scm_nettle_aes_cbc_init() {
#include "scm_nettle_aes_cbc.x"
  aes256_cbc_error_key = scm_from_utf8_symbol("aes256-cbc-error");
}

