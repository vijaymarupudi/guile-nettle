#pragma once
#include <libguile.h>

extern SCM scm_nettle_invalid_bounds;

static inline void validate_bv_arg(int pos, SCM bv, SCM scm_start, SCM scm_end, size_t minimum, char** out, size_t* length) {
#define FUNC_NAME "validate_bv_arg"
  SCM_VALIDATE_BYTEVECTOR(pos, bv);
  *out = SCM_BYTEVECTOR_CONTENTS(bv);
  
  size_t start, end;
  if (SCM_UNBNDP(scm_start)) {
    start = 0;
  } else {
    start = scm_to_size_t(scm_start);  
  }

  if (SCM_UNBNDP(scm_end)) {
    end = SCM_BYTEVECTOR_LENGTH(bv);
  } else {
    end = scm_to_size_t(scm_end);  
  }
  
  if (start > end)
    {
      scm_out_of_range(NULL, scm_start);
    }

  if ((end - start) < minimum)
    {
      scm_out_of_range(NULL, scm_end);
    }

  *out = *out + start;
  *length = end - start;
}
#undef FUNC_NAME
