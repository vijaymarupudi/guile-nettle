#include <libguile.h>
#include <nettle/sha1.h>
#include <nettle/sha2.h>
#include <stdbool.h>
#include "scm_nettle.h"


SCM_DEFINE(scm_nettle_bytevector_to_sha1, "bytevector->sha1", 1, 2, 0, (SCM bv, SCM scm_start, SCM scm_end), "Hash bytevector using SHA1")
{
  size_t len;
  char* buf;
  validate_bv_arg(1, bv, scm_start, scm_end, 0, &buf, &len);
  SCM ret_bv = scm_c_make_bytevector(SHA1_DIGEST_SIZE);
  struct sha1_ctx ctx;
  sha1_init(&ctx);
  sha1_update(&ctx, len, buf);
  sha1_digest(&ctx, SHA1_DIGEST_SIZE, SCM_BYTEVECTOR_CONTENTS(ret_bv));
  return ret_bv;
}

SCM_DEFINE(scm_nettle_bytevector_to_sha256, "bytevector->sha256", 1, 2, 0, (SCM bv, SCM scm_start, SCM scm_end), "Hash bytevector using SHA256")
{
  size_t len;
  char* buf;
  validate_bv_arg(1, bv, scm_start, scm_end, 0, &buf, &len);
  SCM ret_bv = scm_c_make_bytevector(SHA256_DIGEST_SIZE);
  struct sha256_ctx ctx;
  sha256_init(&ctx);
  sha256_update(&ctx, len, buf);
  sha256_digest(&ctx, SHA256_DIGEST_SIZE, SCM_BYTEVECTOR_CONTENTS(ret_bv));
  return ret_bv;
}



void scm_nettle_sha_init() {
#include "scm_nettle_sha.x"
}
