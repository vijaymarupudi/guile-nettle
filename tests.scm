(use-modules (nettle base64)
             (nettle sha)
             (nettle hmac-sha256)
             (nettle base16)
             (nettle pbkdf2-hmac-sha256)
             (nettle yarrow256)
             (srfi srfi-64)
             (rnrs bytevectors)
             (nettle aes))

(test-begin "base64-encoding")

(for-each
 (lambda (str)
   (test-equal str (base64->string (string->base64 str))))
 '("testing" "testing1" "testing11"))

(test-end "base64-encoding")

(test-begin "sha1-test")

(for-each
 (lambda (str final)
   (test-equal (bytevector->base16-string (bytevector->sha1 (string->utf8 str))) final))
 '("1" "2" "3" "4")
 '("356a192b7913b04c54574d18c28d46e6395428ab"
   "da4b9237bacccdf19c0760cab7aec4a8359010b0"
   "77de68daecd823babbb58edb1c8e14d7106e83bb"
   "1b6453892473a467d07372d45eb05abc2031647a"))

(test-end "sha1-test")

(test-begin "hmac-sha265-test")

(let* ((str (string->utf8 "testing"))
       (key (string->utf8 "secret")))
  (test-equal
      "79ee8ebb044e31f5ec95e87202d1c61cad14703847a5edafe82f36018760f915"
    (bytevector->base16-string (bytevector->hmac-sha256 str 0 (bytevector-length str) key))))

(test-end "hmac-sha265-test")

(test-begin "pbkdf2-hmac-sha256-test")

(let ((input (string->utf8 "passwd"))
      (salt (string->utf8 "salt"))
      (out-bv (make-bytevector 64)))
  (pbkdf2-hmac-sha256! 1 input 0 (bytevector-length input)
                      salt 0 (bytevector-length salt)
                      out-bv 0 (bytevector-length out-bv))
  (test-equal "55ac046e56e3089fec1691c22544b605f94185216dde0465e68b9d57c20dacbc49ca9cccf179b645991664b39d77ef317c71b845b1e30bd509112041d3a19783"
    (bytevector->base16-string out-bv)))

(test-end "pbkdf2-hmac-sha256-test")

(test-begin "yarrow256-test")

(let* ((seed (make-bytevector 1000 0))
       (ctx (make-yarrow256-ctx seed 0 (bytevector-length seed)))
       (out-bv (make-bytevector 25)))
  (yarrow256-random! ctx out-bv 0 (bytevector-length out-bv))
  (test-equal #vu8(194 25 248 75 208 184 167 210 44 159 225 212 212 8 241 127 149 139 109 130 70 87 17 99 235) out-bv))

(test-end "yarrow256-test")


(test-begin "aes256-test")

(test-equal "5c9d844ed46f9885085e5d6a4f94c7d7"
  (let* ((ctx (make-aes256-ctx))
         (key (make-bytevector 256))
         (input (base16-string->bytevector "014730f80ac625fe84f026c60bfd547d"))
         (outbv (make-bytevector (bytevector-length input))))
    (aes256-set-encrypt-key! ctx key)
    (aes256-encrypt! ctx input 0 (bytevector-length input)
                     outbv 0 (bytevector-length outbv))
    (bytevector->base16-string outbv)))

(test-equal "secret"
  (utf8->string
   (aes256-cbc-pbkdf2-hmac-sha255-decrypt-bytevector "testing"
                                                     "testing"
                                                     (aes256-cbc-pbkdf2-hmac-sha255-encrypt-bytevector "testing"
                                                                                                       "testing"
                                                                                                       (string->utf8 "secret")))))

(test-end "aes256-test")


