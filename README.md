## guile-nettle

A package with the cryptography primitives from the nettle library.

Currently, this package contains the primitives necessary for 

* secure random number generation (yarrow256, can be used to generate password salts)
* secure password hashing (pbkdf2-hmac-sha256)
* JSON Web Tokens (hmac-sha256)
* base64 and base16 (hex) encoding

Documentation is currently in process, but see `tests.scm` for usage.


### pbkdf2-hmac-sha256

If you are going to use this for password hashing, considering using these parameters for security.

* NIST recommended salt length: 16 bytes [1]
* OWASP recommended iteration count: 310000 [2]
* IETF recommended hash amount to output: 32 bytes [3], explanation [4]


1. https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-132.pdf
2. https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html
3. https://www.ietf.org/rfc/rfc2898.txt
4. https://security.stackexchange.com/questions/110084/parameters-for-pbkdf2-for-password-hashing
